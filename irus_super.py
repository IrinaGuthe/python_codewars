def expression_matter(a, b, c):
    first = a*(b+c)
    second = a*b*c
    third = a+b*c
    fourth = (a+b)*c
    return max(first, second, third, fourth)


def find_needle(haystack):
    hey = haystack.index('needle')
    return "found the needle at position " + str(hey)


def move(position, roll):
    return position + roll * 2


def other_angle(a, b):
    return 180 - (a+b)


def square_sum(numbers):
    result = 0
    for x in numbers:
        result = result + x * x
    return result

# def square_sum(numbers):
    # return sum(x ** 2 for x in numbers)


def seats_in_theater(tot_cols, tot_rows, col, row):
    total = (tot_cols - (col-1)) * (tot_rows - row)
    return total


def monkey_count(n):
    result = []
    for x in range(1, n+1):
        result.append(x)
    return result


def divide_numbers(x, y):
    return x / float(y)


def nth_even(n):
    return n*2-2


def take(arr, n):
    result = arr[:n]
    # result = range(20)[:5]
    return result


def how_many_dalmatians(n):
    dogs = ["Hardly any", "More than a handful!",
            "Woah that's a lot of dogs!", "101 DALMATIONS!!!"]
    if (n <= 10):
        return dogs[0]
    elif (n <= 50):
        return dogs[1]
    elif (n == 101):
        return dogs[3]
    else:
        return dogs[2]


def check_for_factor(base, factor):
    if base % factor == 0:
        return True
    else:
        return False


def even_or_odd(number):
    if number%2 == 0:
        return "Even"
    else:
        return "Odd"


def count_sheeps(arrayOfSheeps):
    return arrayOfSheeps.count(True)


def set_alarm(employed, vacation):
    if (employed == True and vacation == False):
        return True 
    else:
        return False 


def make_upper_case(s):
    return s.upper()


def century(year):
    return (year - 1) // 100 + 1 


def string_to_number(s):
    int_s = int(s)
    return int_s


def summation(num):
    return sum(range(num+1))


def integrate(coefficient, exponent):
    x = exponent + 1 
    return coefficient / x


def get_size(w,h,d):
    suar  = 2 * h * w + 2 * d * h + 2 * w * d  
    volar = w * h * d
    return [volar, suar]


def count_positives_sum_negatives(arr):
    positive_count = 0
    negative_sum = 0
    for n in arr:
        if n > 0:
            positive_count += 1
        elif n < 0:
            negative_sum += n
    return [positive_count, negative_sum]


def bool_to_word(boolean):
    if boolean == True:
        return 'Yes'
    else:
        return 'No'


def final_grade(exam, projects):
    if exam > 90 or projects > 10:
        return 100
    elif exam > 75 and projects >= 5:
        return 90
    elif exam > 50 and projects >= 2:
        return 75
    else:
        return 0


def correct(string):
    result = string.replace ('5', 'S')
    result = result.replace ('0', 'O')
    result = result.replace ('1', 'I')
    return result
    


# print(expression_matter(1,2,3))
# print(find_needle(['hay', 'junk', 'hay', 'hay', 'moreJunk', 'needle', 'randomJunk']))
# print(move(3, 6))
# print(other_angle(30,60))
# print(square_sum([1, 2]))
# print(seats_in_theater(16,11,5,3))
# print(monkey_count(20))
# print(divide_numbers(7, 3))
# print(nth_even(15))
# print(take([0, 1, 2, 3, 5, 8, 13], 3))
# print(how_many_dalmatians(26))
# print(check_for_factor(9, 2))
# print(even_or_odd(9))
# print(count_sheeps([True,  True,  True,  False, True,  True,  True,  True , True,  False, True,  False, True,  False, False, True ,
# True,  True,  True,  True , False, False, True,  True]))
# print(set_alarm(True, True))
# print(make_upper_case("hello"))
# print(century(1900))
# print(string_to_number("1234"))
# print(summation(100))
# print(integrate(3,2))
# print(get_size(4,2,6))
# print(count_positives_sum_negatives([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]))
# print(bool_to_word(False))
# print(final_grade(85,5))
print(correct("L0ND0N"))